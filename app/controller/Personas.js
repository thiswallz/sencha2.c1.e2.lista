Ext.define('Ejemplo1List.controller.Personas', {
    extend: 'Ext.app.Controller',
    //load stores
    config: {
        refs: {
            listaPersonas: 'listapersonas'        
        },
        control: {
            'button[action=btnVolver]': {
                tap: 'volverLista'
            },      
            listaPersonas: {
                accionTab: 'accionTab'
            }        
        }
    },
    launch: function() {
        //console.log(this);
    },
    volverLista: function(){
        Ext.Viewport.animateActiveItem(this.getListaPersonas(),{type: 'slide', direction: 'right'});
    },
    accionTab: function(view, record){
        console.log(record.get('edad'))
        Ext.Viewport.animateActiveItem({xtype : 'persona', nombre: record.get('nombre')}, {type: 'slide', direction: 'left'});
    }
});