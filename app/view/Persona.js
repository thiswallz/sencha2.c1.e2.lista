Ext.define('Ejemplo1List.view.Persona', {
    extend: 'Ext.form.Panel',
    alias: "widget.persona",    
    requires: [
        'Ext.TitleBar',
        'Ext.form.FieldSet',
        'Ext.form.Number', 
        'Ext.Label'    
    ],
    initialize: function() {
        this.callParent();
        var me = this;
        this.down('#nombreId').setValue(me.nombre)
    },
    config: {
        fullscreen : true,        
        layout: 'hbox',
        items: [{
            xtype : 'toolbar',
            docked: 'top',
            items: [{
                xtype: 'button',
                ui: 'back',
                action: 'btnVolver',
                text:'Volver',
                itemId: 'volverId'
            }]
        },{
            xtype: 'fieldset',
            title: 'Persona',
            items: [{
                label : 'Nobre',
                xtype: 'textfield',
                itemId: 'nombreId',
                name: 'nombre'              
            }]
        }]
    }
});
