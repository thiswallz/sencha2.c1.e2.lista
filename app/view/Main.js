Ext.define('Ejemplo1List.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Lista',
                iconCls: 'action',
                items: [
                    {
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Lista Personas'
                    },
                    {
                        xtype: 'listapersonas'                    
                    }
                ]
            }
        ]
    }
});
