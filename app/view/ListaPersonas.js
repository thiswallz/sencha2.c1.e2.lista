Ext.define('Ejemplo1List.view.ListaPersonas', {
    extend: 'Ext.Container',
    alias: "widget.listapersonas",    
    requires: [
        'Ext.TitleBar', 
        'Ext.dataview.List',
        'Ext.data.proxy.Ajax',
        'Ext.data.Store',
        'Ext.util.DelayedTask'
    ],
    onAccionTap: function () {
        var me = this;
        var record = me.down('#listaId').getSelection()[0];
        if(!record){
            Ext.Msg.alert('Lista', "Selecciona un registro");
            return;
        }
        var task = Ext.create('Ext.util.DelayedTask', function () {
            me.fireEvent('accionTab', me, record);
        });
        //pequeño delay para simular un loading
        task.delay(500);  
    },
    config: {
        listeners: [{
            delegate: '#accionId',
            event: 'tap',
            fn: 'onAccionTap'            
        }],
        scrollable: true,
        width: '100%',
        height: '100%',
        layout: 'hbox',
        items: [{
            xtype : 'toolbar',
            docked: 'top',
            items: [
                { xtype: "spacer" },
                {
                    xtype: 'button',
                    ui: 'action',
                    iconCls: 'action',
                    iconMask: true,
                    text:'Accion',
                    itemId: 'accionId'                
                }
            ]
        },
        {
            xtype: 'list',
            itemId: 'listaId',
            flex: 2,
            itemTpl: '{nombre}',
            data: [
                { nombre: 'Juan' },
                { nombre: 'Raul' },
                { nombre: 'Pedro' },
                { nombre: 'Diego' }
            ]      
        }]
    }
});
